package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Arc;
import javafx.scene.layout.Pane;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane paneObj = new Pane();
        Polyline polyline1 = new Polyline();
        paneObj.getChildren().add(polyline1);
        polyline1.setStroke(Color.BLACK);
        ObservableList<Double> list = polyline1.getPoints();
        double x1 = 50.0;
        double y1 = 200.0;
        double y2 = 25.0;
        double x3 = 130.0;
        list.addAll(x1, y1, x1, y2, x3, y2, x3, y1 * .60);
        Polyline polyline2 = new Polyline();
        paneObj.getChildren().add(polyline2);
        polyline2.setStroke(Color.BLACK);
        ObservableList<Double> list2 = polyline2.getPoints();
        list2.addAll((x1 + x3) * .5, y1 * .50, x3, y1 * .25, x3 + (x3 - x1) * .50, y1 * .50);
        Polyline polyline3 = new Polyline();
        paneObj.getChildren().add(polyline3);
        polyline3.setStroke(Color.BLACK);
        ObservableList<Double> list3 = polyline3.getPoints();
        list3.addAll((x1 + x3) * .6, y1 * .80, x3, y1 * .60, x3 + (x3 - x1) * .3, y1 * .80);
        Circle circleObj = new Circle(x3, y1 * .25, 15);
        circleObj.setFill(Color.WHITE);
        circleObj.setStroke(Color.BLACK);
        paneObj.getChildren().add(circleObj);
        Arc arcObj = new Arc(x1, y1 + 1, 25, 15, 0, 180);
        arcObj.setFill(Color.WHITE);
        arcObj.setStroke(Color.BLACK);
        paneObj.getChildren().add(arcObj);
        Scene sceneObj = new Scene(paneObj, 200, 200);
        primaryStage.setScene(sceneObj);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
