package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.VBox;

import java.util.Scanner;


public class Main extends Application {

    private Rectangle rectangleOne, rectangleTwo;


    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane pane = new Pane();
        VBox vBox = new VBox(50);
        vBox.setPadding(new Insets(70, 55, 55, 70));
        Scanner sc = new Scanner(System.in);
        System.out.println("x");
        Double name0 = sc.nextDouble();
        System.out.println("y");
        Double name1 = sc.nextDouble();
        System.out.println("w");
        Double name2 = sc.nextDouble();
        System.out.println("h");
        Double name3 = sc.nextDouble();
        rectangleOne = createRectangle(name0, name1, name2, name3);

        System.out.println("x");
        Double name4 = sc.nextDouble();
        System.out.println("y");
        Double name5 = sc.nextDouble();
        System.out.println("w");
        Double name6 = sc.nextDouble();
        System.out.println("h");
        Double name7 = sc.nextDouble();
        rectangleTwo = createRectangle(name4, name5, name6, name7);

        String output = "";

        if (checkInside(rectangleOne, rectangleTwo) || checkInside(rectangleTwo, rectangleOne))
            output += "Rectangle is contained in another";

        else if (checkOverlapping(rectangleOne, rectangleTwo))
            output +="overlapping";

        else
            output += "do not overlap";

        pane.getChildren().addAll(rectangleOne, rectangleTwo);
        vBox.getChildren().addAll(pane, new Text(20, 0, output));
        Scene scene = new Scene(vBox);
        stage.setTitle("Check Overlaping");
        stage.setScene(scene);
        stage.show();
    }

    private Rectangle createRectangle(double x, double y, double w, double h) {
        Rectangle rectangle = new Rectangle(x, y, w, h);
        rectangle.setFill(Color.YELLOW);
        rectangle.setStroke(Color.RED);
//        rectangleTwo = new Rectangle(Double.parseDouble(getParameters().getNamed().get("Circle2x")), Double.parseDouble(getParameters().getNamed().get("Circle2y")), Double.parseDouble(getParameters().getNamed().get("Circle2width")), Double.parseDouble(getParameters().getNamed().get("Circle2Height")));
//        rectangleTwo.setFill(Color.YELLOW);
//        rectangleTwo.setStroke(Color.RED);
        return rectangle;
    }

    public boolean checkInside(Rectangle r1, Rectangle r2) {
        return (r2.getY() + r2.getHeight() <= r1.getY() + r1.getHeight()) && (r2.getX() + r2.getWidth() <= r1.getX() + r1.getWidth()) && (r2.getX() > r1.getX()) && (r2.getY() > r1.getY());
//        return (r2.getY() + r2.getHeight()) == (r1.getY() + r1.getHeight());
    }

    public boolean checkOverlapping(Rectangle r1, Rectangle r2) {
        return getDistance(r1.getX(), r2.getX() + r2.getWidth()) < r1.getWidth() + r2.getWidth() && getDistance(r1.getY(), r2.getY() + r2.getHeight()) < r1.getHeight() + r2.getHeight();
    }

    private double getDistance(double p1, double p2) {
        return Math.sqrt(Math.pow(p2 - p1, 2));
    }
}

